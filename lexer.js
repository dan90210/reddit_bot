var IDENTIFIER = 'IDENTIFIER';
var PRINT = 'PRINT';
var IMAGES = 'IMAGES';
var TITLES = 'TITLES';
var LEFT_PARENTHESIS = 'LEFT_PARENTHESIS';
var RIGHT_PARENTHESIS = 'RIGHT_PARENTHESIS';
var COMMA = 'COMMA';
var INTEGER = 'INTEGER';
var STRING = 'STRING';
var EOF = 'EOF';
var END = 'END';
var ASSIGN = 'ASSIGN';
var EQUAL = 'EQUAL';
var WHILE = 'WHILE';
var LESS = 'LESS';
var LESSEQUAL = 'LESSEQUAL';
var GREATER = 'GREATER';
var GREATEREQUAL = 'GREATEREQUAL';

function lex(source) {
    var tokens = [];
    var tokenSoFar = '';
    var i = 0;

    function has(regex) {
        return source.charAt(i).match(regex);
    }

    function devour() {
        tokenSoFar += source.charAt(i);
        i += 1;
    }

    function emit(type) {
        tokens.push({type: type, source: tokenSoFar});
        tokenSoFar = '';
    }

    while (i < source.length) {
        if (has(/=/)) {
            devour();
            if (has(/=/)) {
                devour();
                emit(EQUAL);
            } else {
                emit(ASSIGN);
            }
        } else if (has(/</)) {
            devour();
            if (has(/=/)) {
                emit(LESSEQUAL);
            } else {
                emit(LESS);
            }
        } else if (has(/>/)) {
            devour();
            if (has(/=/)) {
                emit(GREATEREQUAL);
            } else {
                emit(GREATER);
            }
        } else if (has(/\(/)) {
            devour();
            emit(LEFT_PARENTHESIS);
        } else if (has(/\)/)) {
            devour();
            emit(RIGHT_PARENTHESIS);
        } else if (has(/\d/)) {
            while (has(/\d/)) {
                devour();
            }
            emit(INTEGER);
    	} else if (has(/"/)) {
            devour();
            tokenSoFar = "";
            while (!has(/"/)){
                devour();
            }
            emit(STRING);
	        devour();
            tokenSoFar = "";
        } else if (has(/,/)) {
            devour();
            emit(COMMA);
        } else if (has(/[a-zA-Z]/)) {
            while (has(/[a-zA-Z0-9]/)) {
                devour();
            }
            
            // methods
            if (tokenSoFar == 'print') {
                // this one might be wrong!?!?!?!?!
                emit(PRINT);
            } else if (tokenSoFar == 'getImage'){
                emit(IMAGES);
            } else if (tokenSoFar == 'getTitles'){
                emit(TITLES);
            } else if (tokenSoFar == 'while') {
                emit(WHILE);
            } else if (tokenSoFar == 'end') {
                emit(END);
	    } else{
                emit(IDENTIFIER);
            }
        
        }  else if (has(/\s/)) {
            devour();
            tokenSoFar = '';
        }
        else {
            throw 'ack! [' + tokenSoFar + ']';
        }
    }

    emit(EOF);

    return tokens;
}
