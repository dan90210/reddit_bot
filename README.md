# README #

Authors: Dan Hessler, Sean Hupfauf

This README would normally document whatever steps are necessary to get your application up and running.

### DESCRIPTION ###

Our language is essentially a reddit bot interface. Through our language you will be able to Retrieve the links from a certain subreddit. See current trending subreddits. Download all images on a page. Display the posts and comments of said post.

### GRAMMAR ###

IDENTIFIER ASSIGN expression

PRINT expression

WHILE condition
code to run
END

### built in methods ###

`getImage "url"`
    Gets the image from certain url

`print "stuff to print"`
    prints whatever is in the quotes to the console

### unimplemented methods ###

`getPost()`
    Gets posts from a certain page

`getComments()`
    Gets comments on a certain post

### HOW TO RUN ###

The language shell will be web based, similar to what Chris did in class
Use any browser other than Firefox as this was giving us odd errors.

To open our page you can run:
`open index.html` 
from the command line, or open it like you would any other way

node must be installed

```
npm install --save cheerio
npm install --save request
npm install xhr
```

### CURRENT STATE ###

In the current state of our language, there is very limited functionality

Our plan was to have a method called getPosts that would return the top posts from [Reddit](http://www.reddit.com/r/all)
We are able to get these posts by running node from the command line with the command
`node server.js`
This creates an html file that can then be opened in a browser and shows all the posts
It cannot currently be run from our interpreter as we were unable to find a way to run node code from an html page and we also tried running the html page on a local node server but ran into problems there as well. 
We show the reddit.html page in a text box on our interpreter to show what it could do.

Firefox was giving us odd errors so we reccomend using chrome or safari.

### Code Example ###
```
url = "https://i.redditmedia.com/ZQLhsLYljBSH-nrsLHPwcukKSlLEnrZpu0zn9mvbRM4.jpg?s=7bcb0951a69b8a7eedab598b78c55cba"

getImage url
```

```
while 1 < 10
print 'I'm trapped in an endless loop'
end
```
