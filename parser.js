function parse(tokens) {
    var i = 0;
    
    function has(tokenType) {
        return i < tokens.length && tokens.length && tokens[i].type == tokenType;
    }

    function devour() {
        var token = tokens[i];
        i++;
        return token;
    }

    function program() {
        var statements = [];
        while (!has(EOF)) {
            statements.push(statement());
        }
        return new Block(statements);
    }

    function expression() {
        return relational();
    }

    function conditional() {
    if (!has(END)) {
      throw 'expected END';
    }
    devour(); // eat end
    }

    function loop() {
        devour();
        var condition = expression();
        var statements = [];
        while (i < tokens.length && !has(END)) {
            statements.push(statement());
        }
        if (!has(END)) {
            throw 'expected END';
        }
        devour(); // eat end
        var block = new Block(statements);
        return new ExpressionWhile(condition, block);
    }

    function statement() {
        if (has(IMAGES)) {
            devour();
            // TODO decide what parameters are going ere
            // TODO url
            var url = expression();
            return new StatementImage(url);

        } else if (has(PRINT)) {
            devour();
    	    var message = expression();
            console.log("MESSAGE", message);
            return new StatementPrint(message);
        } else if (has(TITLES)) {
            devour();
            return new StatementTitle();
            
        } else if (has(WHILE)) {
    	    return loop();
        } else if (has(IDENTIFIER)) {
            var idToken = devour();
            if (has(ASSIGN)) {
                devour();
                var rhs = expression();
                console.log('idtoken.source = ' + idToken.source + ' rhs = ' + rhs);
                return new StatementAssignment(idToken.source, rhs);
            } else if (!has(ASSIGN)) {
                throw 'Curses my dude. ' + tokens[i];
            }
        }
        else {
            throw ' you messed up, i dont know: ' + tokens[i].source;
        }
    }

    function relational() {
	    var a = atom();
	while (has(GREATEREQUAL) || has(GREATER) || has(LESSEQUAL) || has(LESS) || has(EQUAL)) {
	      var operator = tokens[i];
	      devour(); // eat operator
	      var b = atom();
	      if (operator.type == GREATEREQUAL) {
		a = new ExpressionMoreOrEqual(a, b);
	      } else if (operator.type == GREATER) {
		a = new ExpressionMore(a, b);
	      } else if (operator.type == LESSEQUAL) {
		a = new ExpressionLessOrEqual(a, b);
	      } else if (operator.type == LESS) {
		a = new ExpressionLess(a, b);
	      } else if (operator.type == EQUAL) { 
		a = new ExpressionEqual(a, b);
	      }
	}
    	return a;
    }

    function atom() {
	console.log("TOKEN", tokens[i]);
        if (has(INTEGER)) {
            var token = devour();
            return new ExpressionIntegerLiteral(parseInt(token.source));
	 } else if (has(STRING)) {
            var token = devour();
	    console.log(token);
            var tmp = new ExpressionStringLiteral(token.source);	
	    console.log("TMP", tmp);
	    return tmp;
        } else if (has(IDENTIFIER)) {
            var token = devour();
            return new ExpressionVariableReference(token.source);
        } else if (has(LEFT_PARENTHESIS)) {
            devour();
            var token = devour();
            if (!has(RIGHT_PARENTHESIS)) {
                throw 'unbalanced parenthesis';
            }
            devour();
            return new ExpressionVariableReference(token.source);
        } else {
            throw 'Unknown expression: ' + tokens[i].source;
        }

    }

    return program();
}
