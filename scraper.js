var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
var express = require('express');
var app = express();
var path = require('path');
var http = require('http');

request("https://www.reddit.com/r/all", function(error, response, body) {
    if(error) {
        console.log("Error: " + error);
    }
    console.log("Status code: " + response.statusCode);

    var $ = cheerio.load(body);

    $('div#siteTable > div.link').each(function( index ) {
        var title = $(this).find('p.title > a.title').text().trim();
        var time = $(this).find('p.tagline').text().trim();
        var user = $(this).find('a.author').text().trim();

        
        console.log("Title: " + title);
        console.log("Author: " + user);
        console.log("Time: " + time);
        // possibly wrap this part in <li> tags???
        fs.appendFileSync('reddit.html', '<li>' + title + '</li>' + '\n');

    });

    
});

app.get('/',function(req,res) {
    res.sendFile(path.join(__dirname+'/index.html'));
});

app.get('/',function(req,res) {
    res.sendFile(path.join(__dirname+'/reddit.html'));
});

app.get('/ast',function(req,res) {
    res.sendFile(path.join(__dirname+'/ast.js'));
});

app.get('/parser',function(req,res) {
    res.sendFile(path.join(__dirname+'/parser.js'));
});

app.get('/lexer',function(req,res) {
    res.sendFile(path.join(__dirname+'/lexer.js'));
});

app.listen(3000);

console.log('listening on port 3000')

