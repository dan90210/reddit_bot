function Block(statements) {
  this.evaluate = function(env) {
    statements.forEach(statement => statement.evaluate(env));
  }
}

function StatementPrint(messageExpression) {
    // TODO we need to figure out what we are supposed to pass in as the env
  this.evaluate = function(env) {
    var message = messageExpression.evaluate(env); 
    console.log(message);
    // this redirects us, but it sort of works?
   // document.write(message);

  }
}

function ExpressionIntegerLiteral(literal) {
  this.evaluate = function(env) {
    return literal;
  }
}

// TODO this is where the error is
function ExpressionStringLiteral(literal) {
  this.evaluate = function(env) {
    return literal;
  }
}

function ExpressionVariableReference(id) {
    this.evaluate = function(env) {
        return env[id];
    }
}

function ExpressionGreater(a, b) {
  this.evaluate = function(env) {
    var valueA = a.evaluate(env);
    var valueB = b.evaluate(env);
    return valueA > valueB;
  }
}

function ExpressionGreaterOrEqual(a, b) {
  this.evaluate = function(env) {
    var valueA = a.evaluate(env);
    var valueB = b.evaluate(env);
    return valueA >= valueB;
  }
}

function ExpressionLess(a, b) {
  this.evaluate = function(env) {
    var valueA = a.evaluate(env);
    var valueB = b.evaluate(env);
    return valueA < valueB;
  }
}

function ExpressionLessOrEqual(a, b) {
  this.evaluate = function(env) {
    var valueA = a.evaluate(env);
    var valueB = b.evaluate(env);
    return valueA <= valueB;
  }
}

function ExpressionEqual(a, b) {
    this.evaluate = function(env) {
        var valueA = a.evaluate(env);
        var valueB = b.evaluate(env);
        return valueA == valueB;
    }
}

function ExpressionWhile(condition, block) {
  this.evaluate = function(env) {
    while (condition.evaluate(env) != 0) {
      block.evaluate(env);
    }
  }
}

function StatementImage(messageExpression) {
  this.evaluate = function(env) {
    var url  = messageExpression.evaluate(env); 

    show_image(url, 500, 500, 'test')
  }
}

function show_image(src, width, height, alt) {
    var img = document.createElement("img");
    img.src = src;
    img.width = width;
    img.height = height;
    img.alt = alt;

    document.body.appendChild(img);

}

function StatementTitle(){
    document.getElementsByTagName(a);
}

function StatementAssignment(id, rhsExpression) {
    console.log('i am in the ast for statementassign');
    this.evaluate = function(env) {
        env[id] = rhsExpression.evaluate(env);
    }
}

